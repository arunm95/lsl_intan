﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using LSL;

namespace LSL_Intan_Link
{
    class LSL_Send
    {
        liblsl.StreamOutlet outlet;
        public LSL_Send()
        {
            // create stream info and outlet
            liblsl.StreamInfo info = new liblsl.StreamInfo("Intan", "EEG", 6, 25000, liblsl.channel_format_t.cf_float32, "Intan I/O 1.0A");
            outlet = new liblsl.StreamOutlet(info);
        }

        public int Send(float[,] data, double timestamp) 
        {
            // send data in multiplexed chunks, timestamps are created at time of reading data, to reduce lag.
            outlet.push_chunk(data, timestamp);
            return 0;
        }
    }
}
