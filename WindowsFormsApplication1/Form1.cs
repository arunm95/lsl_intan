﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using USB_Scope;
using LSL;
using LSL_Intan_Link;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {

        USB_Scope.USBSource dev;
        Boolean SynthData = false;
        Boolean unlink = false;
        public Form1()
        {
            InitializeComponent();

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void Form1_FormClosing(object sender, EventArgs e)
        {
            dev.Stop();
            dev.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            label3.Text = "Opening Connection to Intan EEG Board";
            dev = new USBSource();
            int firmwareID1 = 0;
            int firmwareID2 = 0;
            int firmwareID3 = 0;
            try {
                dev.Open(ref firmwareID1, ref firmwareID2, ref firmwareID3);
            } catch (UsbException z)
            {
                label3.Text = z.Message.ToString();
                if (SynthData)
                {
                    label3.Text = "Proceeding to synthetic neural data mode.";
                    dev.SynthDataMode = true;
                    label3.Text = "Generating neural data.";
                    double time = liblsl.local_clock();
                    Queue<USBData> plotQueue = new Queue<USBData>();
                    Queue<time_inc> saveQueue = new Queue<time_inc>();
                    LSL_Send lslComm = new LSL_Send();
                    float[,] dataArray;
                    ushort[] auxArray = new ushort[750];
                    label3.Text = "Opening LSL link...";
                    while (!unlink && (liblsl.local_clock() - time >= 0.03)) {
                        dev.CheckForUsbData(plotQueue, saveQueue);
                        time = liblsl.local_clock();
                        label3.Text = "Sending data.";
                        time_inc dat = saveQueue.Dequeue();
                        dataArray = new float[16, 750];
                        label3.Text = "Sending data..";
                        dat.data.CopyToArray(dataArray, auxArray);
                        lslComm.Send(dataArray, dat.timestamp);
                        label3.Text = "Sending data...";
                    }
                }
            }
            dev.Close();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                SynthData = true;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            unlink = true;
        }
    }
}
